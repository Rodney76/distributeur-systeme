#!/bin/bash


phelp()
{
	echo "stopdemo : arret de la demo phytec"
	echo "godemo   : lancement de la demo phytec"
	echo "tscreen  : lancement du touch screen"
}

getdistrib()
{
	scp root@192.168.0.48:/Desktop/DOC_DEV_DISTRIBUTEUR/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR /home
}


stopdemo()
{
	systemctl stop phytec-qtdemo.service;
}

godemo()
{
	/usr/bin/QtDemo -qws &
}

tscreen()
{
	/usr/bin/eGTouchD &
}



#--------------------------------------------------------------------------
# Verif si cartes SD presentes et montees, sinon les monte
if cat /proc/mounts | grep -F " /media/sdcard " > /dev/null; then
	echo " Carte SD1 deja montee"
else
	mount -o noatime /dev/mmcblk0p1 /media/sdcard
	if [ $? != 0 ]; then
		echo "  Montage SD card 1 echoue :" $?
	else
		echo "  Montage SD card 1 OK"
	fi
fi


#--------------------------------------------------------------------------
VERSION_OPTIWEB_UST="0.0.1"
#2017-06-30 version 0.0.1 - script de mise a l heure depuis ntpdate

echo "update_sys_time $VERSION_OPTIWEB_UST"

	echo " Demarrage NTPDATE..."
	ntpdate -s 0.fr.pool.ntp.org 1.fr.pool.ntp.org
# verifier si mis a lheure. si oui alors faire deux fois: "hwclock -w" pour mise a l heure du rtc
	if [ $? = 0 ]; then
		echo " ntpdate ok, mise a l'heure du RTC"
		hwclock -w
		hwclock -w
	else
		echo " ntpdate indisponible, lecture heure RTC"
		hwclock -s
	fi
	echo "  date systeme = $(date)"
	echo "  date hardware = $(hwclock -r)"
#exit 0

#--------------------------------------------------------------------------



sleep 10
echo "--> cursor blink"
/bin/sh -c "echo 0 > /sys/class/graphics/fbcon/cursor_blink"

sleep 2
echo "--> eGTouchD"
/usr/bin/eGTouchD

sleep 5
echo "--> restart getty@tty1"
#/bin/sh -c "systemctl restart getty@tty1"

sleep 1
echo "--> go distributeur"
cd home

# control du brightness
echo 7 > /sys/class/backlight/backlight.10/brightness

# mise a l'heure fuseau horaire                                                                                      
timedatectl set-timezone Europe/Paris                                                                                    

                                                                                                                                             
# preparer le distributeur a faire du reverse ssh       
source reverse_ssh.sh                                                   


./DISTRIBUTEUR


