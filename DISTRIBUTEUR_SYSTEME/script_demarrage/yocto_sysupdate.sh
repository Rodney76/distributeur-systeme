#!/bin/sh

# distrib_sysupdate : script a placer sur carte SD en FAT !
# permet de mettre a jour automatiquement kernel et rootfs si presents sur la SD
# placer dans le dossier updates\ avec le linuximage.x.x.x.ow et rootubifs.x.x.x.ow

# A.S script lance par barebox

IMAGEFILE=""
KERNEL_FILE=""
DTB_FILE=""
UPDATEPATH="/mnt/disk/updates"
UPDATEDIR="/mnt/disk/updates/"
CURVERPATH="/mnt/disk/curversion"

#-------- FLASHAGE KERNEL -----------------------------------
cd $UPDATEPATH
for i in dist_kernel.*.ow
do
 	KERNEL_FILE=$i
done

if [ -f $i ]; then
	echo "Found a kernel update: $i"

	for k in dist_dtb.*.ow
	do
	 	DTB_FILE=$k
	done

	if [ -f $k ]; then
		echo "Found a dtb update: $k"
		echo " Flashing the new kernel and dtb"
		erase /dev/nand0.kernel.bb
		cp $i /dev/nand0.kernel.bb

		#ubiupdatevol /dev/nand0.root.ubi.kernel $i

		if [ $? = 0 ]; then
			echo " kernel OK!"
		else
			echo " Erreur de flashage kernel !"
		fi

		erase /dev/nand0.oftree.bb
		cp $k /dev/nand0.oftree.bb

		#ubiupdatevol /dev/nand0.root.ubi.oftree $k

		if [ $? = 0 ]; then
			echo " dtb OK!"
		else
			echo " Erreur de flashage dtb !"
		fi
	fi
fi

mkdir 
#-------- RECHERCHE ROOTFS/IPK -------------------------------
cd $UPDATEPATH
for i in dist_rootfs.*.ow
do
 	IMAGEFILE=$i
done

if [ -f $i ]; then
	echo "Found a rootfs update: $i"
	echo " Flashing new root FS, please wait..."
	#ubiformat /dev/nand0.root
	#ubiattach /dev/nand0.root
	#ubimkvol -t dynamic /dev/nand0.root.ubi root 0

	ubiformat /dev/nand0.root 
	ubiattach /dev/nand0.root 
	ubimkvol  /dev/nand0.root.ubi root 0

	#cp $i /dev/ubi0.root

	cp $i /dev/nand0.root.ubi.root

	if [ $? = 0 ]; then
		echo " Rootfs OK!"

		#-----------------------------------------------------	
		IMAGEFILE=""
		for i in dist_ipk.*.ipk
		do
			IMAGEFILE=$i
		done
		if [ ! -f $i ]; then
			cd $CURVERPATH
			for i in dist_ipk.*.ipk
			do
				IMAGEFILE=$i
			done
			if [ -f $IMAGEFILE ]; then
				cp $IMAGEFILE $UPDATEDIR
			fi
		fi

		#-----------------------------------------------------
		IMAGEFILE=""
		for i in dist_init_distrib.*.sh
		do
			IMAGEFILE=$i
		done
		if [ ! -f $i ]; then
			cd $CURVERPATH
			for i in dist_init_distrib.*.sh
			do			
				IMAGEFILE=$i
			done
			if [ -f $IMAGEFILE ]; then
				cp $IMAGEFILE $UPDATEDIR
			fi
		fi
	else
		echo " Erreur de flashage rootfs !"
	fi
fi

echo " yocto_sysupdate termine!"
exit

