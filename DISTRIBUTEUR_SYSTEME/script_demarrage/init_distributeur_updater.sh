#!/bin/sh
VERSION_OPTIWEB_ICU="0.0.2"

# script qui met a jour le script de mise a jour


echo "init_distributeur_updater $VERSION_OPTIWEB_ICU"
echo "  Verifie si il existe une mise a jour pour init_distributeur spe HX"

if cat /proc/mounts | grep -F " /media/sdcard " > /dev/null; then
	echo "  Carte SD1 deja montee"
else
	mount -o noatime /dev/mmcblk0p1 /media/sdcard
	if [ $? != 0 ]; then
		echo "  Montage SD card 1 echoue :" $?
	else
		echo "  Montage SD card 1 OK"
	fi
fi

# verifie si mise a jour disponible
ls /media/sdcard/updates/init_distributeur* > /dev/null
if [ $? = 0 ]; then
#if [ -f /media/sdcard/updates/init_distributeur* ]; then
		echo "Copie nouveau script init_distributeur"
		cp /media/sdcard/updates/init_distributeur* /etc/init.d/init_distributeur
		if [ ! -d /media/sdcard/curversion ]; then
			mkdir /media/sdcard/curversion
		fi
		if [ ! -d /media/sdcard/lastversion ]; then
			mkdir /media/sdcard/lastversion
		fi
	ls /media/sdcard/curversion/init_distributeur* > /dev/null
	if [ $? = 0 ]; then
		mv -f /media/sdcard/curversion/init_distributeur* /media/sdcard/lastversion/
	fi
		mv -f /media/sdcard/updates/init_distributeur* /media/sdcard/curversion/
		
	else
		echo "  *** Pas de MAJ dispo sur SD card"
	fi
# verifie si init_distributeur est bien present, sinon reprend celui de curversion si existe
	if [ ! -f /etc/init.d/init_distributeur ]; then
		if [ -f /media/sdcard/curversion/init_distributeur* ]; then
			echo " ! script init_distributeur non present, recup ancienne version"
			cp /media/sdcard/curversion/init_distributeur* /etc/init.d/init_distributeur
		else
			echo " ! script init_distributeur inexistant - manquant"
		
		fi
	fi
exit 0

