#!/bin/sh
VERSION_OPTIWEB_IC="0.0.9"
echo "Init centrale spe HX version $VERSION_OPTIWEB_IC"

IMAGEFILE=""
UPDATEPATH="/media/sdcard/updates"
UPDATEDIR="/media/sdcard/updates/"
CURVERPATH="/media/sdcard/curversion"


# Defini port io permettant de ne pas redemarrer alim si u coupure.
echo 56 >> /sys/class/gpio/export 
echo out >> /sys/class/gpio/gpio56/direction 
echo 0 >> /sys/class/gpio/gpio56/value 


# Creation repertoires lastversion et curversion si pas deja fait.
if [ ! -d /media/sdcard/lastversion ]; then
	mkdir /media/sdcard/lastversion
fi
if [ ! -d /media/sdcard/curversion ]; then
	mkdir /media/sdcard/curversion
fi


# MISE A JOUR DU PACKAGE IPK SI DISPONIBLE
#LIST_IPK=$(ls /media/sdcard/updates/cent-optiweb*.ipk)
#ls /media/sdcard/updates/cent-optiweb*.ipk > /dev/null
#if [ $? = 0 ]; then
cd $UPDATEPATH
for i in dist_ipk.*.ipk
do
 	IMAGEFILE=$i
done

if [ -f $i ]; then
#il existe 1 ou plusieurs fichiers, on va prendre le premier uniquement
	echo " Package(s) update optiweb trouve(s) $i"
	ipkg install -force-overwrite $i
	cd $CURVERPATH
	IMAGEFILE=""
	for i in dist_ipk.*.ipk
	do
 		IMAGEFILE=$i
	done
#	ls /media/sdcard/curversion/cent-optiweb*.ipk > /dev/null
	if [ -f $i ]; then
		mv -f /media/sdcard/curversion/dist_ipk.*.ipk /media/sdcard/lastversion/
	fi
	mv -f /media/sdcard/updates/dist_ipk.*.ipk /media/sdcard/curversion/
elif [ ! -f /home/heurtaux ]; then
#heurtaux pas present: rootfs neuf et donc reinstaller le ipk dans curversion
#	ls /media/sdcard/curversion/cent-optiweb*.ipk > /dev/null
	cd $CURVERPATH
	IMAGEFILE=""
	for i in dist_ipk.*.ipk
	do
 		IMAGEFILE=$i
	done
	if [ -f $i ]; then
		#il existe 1 ou plusieurs fichiers, on va prendre le premier uniquement
		echo " Recup ancien Package optiweb"
		ipkg install -force-overwrite $i
	fi
fi


#demarrage du surveilleur cable reseau pour reconnection auto
ifplugd &


#verif si une update systeme vient d etre installee. si oui la copie dans curversion
cd $UPDATEPATH
IMAGEFILE=""
for i in dist_kernel.*.ow
do
 	IMAGEFILE=$i
done
if [ -f $i ]; then
	mv -f /media/sdcard/curversion/dist_kernel.*.ow /media/sdcard/lastversion/
	mv -f /media/sdcard/updates/dist_kernel.*.ow /media/sdcard/curversion/
fi


IMAGEFILE=""
for i in dist_dtb.*.ow
do
 	IMAGEFILE=$i
done
if [ -f $i ]; then
	mv -f /media/sdcard/curversion/dist_dtb.*.ow /media/sdcard/lastversion/
	mv -f /media/sdcard/updates/dist_dtb.*.ow /media/sdcard/curversion/
fi


IMAGEFILE=""
for i in dist_rootfs.*.ow
do
 	IMAGEFILE=$i
done
if [ -f $i ]; then
	mv -f /media/sdcard/curversion/dist_rootfs.*.ow /media/sdcard/lastversion/
	mv -f /media/sdcard/updates/dist_rootfs.*.ow /media/sdcard/curversion/
fi

exit 0

