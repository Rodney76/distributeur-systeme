#!/bin/sh

echo "Creation du fichier distrib.*.tar.bz2"
echo "Se placer dans le repertoire toPack, avec le script pour creer le .bz2"

SRC_SYSTEM_DIR="/home/phyvm/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME"
PACK_DIR="/home/phyvm/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/toPack"
BUILD_DIR="/home/phyvm/Desktop/DOC_DEV_DISTRIBUTEUR/build/distrib-phyBOARD_Wega-Debug"
BITMAPS_DIR="/home/phyvm/Desktop/DOC_DEV_DISTRIBUTEUR/build/distrib-Desktop_Qt_5_3_GCC_64bit-Debug"

# /usr/lib/libQt5SerialPort.so.5
# /lib/systemd/network/eth0.network
# /lib/systemd/system/can0.service
# /etc/systemd/system/basic.target.wants/can0.service



cp $BUILD_DIR/DISTRIBUTEUR $PACK_DIR/home
cp $BITMAPS_DIR/config.txt $PACK_DIR/home
cp $BITMAPS_DIR/default.qss $PACK_DIR/home
cp $BITMAPS_DIR/default_maint.qss $PACK_DIR/home
cp $BITMAPS_DIR/images/* $PACK_DIR/home/images

cp $SRC_SYSTEM_DIR/distrib_start.sh $PACK_DIR/home
cp $SRC_SYSTEM_DIR/eth0.network $PACK_DIR/lib/systemd/network
cp $SRC_SYSTEM_DIR/can0.service $PACK_DIR/lib/systemd/
cp $SRC_SYSTEM_DIR/libQt5SerialPort.so.5 $PACK_DIR/usr/lib

tar jcvf distrib.0.tar.bz2 home lib usr


<<COMMENT
modetest=""
echo "Attention: ow_synchro et programme heurtaux doivent etre compiles tous les deux soit en test, soit en release"
read -p "Version test (SERVEUR DE TEST ACTUEL), test525(FUTUR SERVEUR DE TEST) ou release (PROD)? (taper en toutes lettres) : " modetest

if [ $modetest = "test525" ]; then
	pkgdir="PKG_sources_525"
	builddir="/buildevo525"
	toolsbuilddir="/buildevo525"
elif [ $modetest = "test" ]; then
	pkgdir="PKG_sources_test"
	builddir="/build"
	toolsbuilddir="/build"
elif [ $modetest = "release" ]; then
	pkgdir="PKG_sources"
	builddir="/rbuild"
	toolsbuilddir="/rbuild"
else
	exit
fi


#-----------------------------------------------------



#-----------------------------------------------------
echo -n " Creation du fichier distrib.*.tar.bz2 termine! Copier le paquet sur le ftp (y/n) ?"
read result
if [ ! $result = "y" ]; then exit; fi


echo "copy sur le serveur ftp"

if [ $modetest = "test525" ]; then

	HOST='145.239.195.235'
	USER='ftp.user'

	echo "Copie via SSH sur $HOST patientez..."

	scp $filenama $USER@$HOST:/home/ftp.user/updates/optiwebsw/tempipkfile

	ssh $USER@$HOST mv /home/ftp.user/updates/optiwebsw/tempipkfile /home/ftp.user/updates/optiwebsw/$filenamb

elif [ $modetest = "test" ]; then
	HOST='ftp-tst.optiweb-access.com'
	USER='optiwebadmin'
	PASSWD='eiqu1eeGow3oing0mi'
else
	HOST='ftp-prd.optiweb-access.com'
	USER='optiwebadmin'
	PASSWD='eiqu1eeGow3oing0mi'
fi


if [ ! $modetest = "test525" ]; then
	echo "Copie sur FTP $HOST patientez..."

	ftp -n $HOST 
fi

COMMENT
