#!/bin/sh
VERSION_OPTIWEB_IC="0.0.9"
echo "Init centrale spe HX version $VERSION_OPTIWEB_IC"

IMAGEFILE=""
UPDATEPATH="/media/sdcard/updates"
UPDATEDIR="/media/sdcard/updates/"
CURVERPATH="/media/sdcard/curversion"

#defini port io permettant de ne pas redemarrer alim si u coupure
# pilotage de LED qui servent comme indicateur.

echo 56 >> /sys/class/gpio/export 
echo out >> /sys/class/gpio/gpio56/direction 
echo 0 >> /sys/class/gpio/gpio56/value 
	echo " Demarrage NTPDATE..."
	ntpdate -s 0.fr.pool.ntp.org 1.fr.pool.ntp.org
# verifier si mis a lheure. si oui alors faire deux fois: "hwclock -w" pour mise a l heure du rtc
	if [ $? = 0 ]; then
		echo " ntpdate ok, mise a l'heure du RTC"
		hwclock -w
		hwclock -w
	else
		echo " ntpdate indisponible, lecture heure RTC"
		hwclock -s
	fi
	echo "  date systeme = $(date)"
	echo "  date hardware = $(hwclock -r)"

if [ ! -d /media/sdcard/lastversion ]; then
	mkdir /media/sdcard/lastversion
fi
if [ ! -d /media/sdcard/curversion ]; then
	mkdir /media/sdcard/curversion
fi
# MISE A JOUR DU PACKAGE IPK SI DISPONIBLE
#LIST_IPK=$(ls /media/sdcard/updates/cent-optiweb*.ipk)
#ls /media/sdcard/updates/cent-optiweb*.ipk > /dev/null
#if [ $? = 0 ]; then
cd $UPDATEPATH
for i in cent-optiweb*.ipk
do
 	IMAGEFILE=$i
done

if [ -f $i ]; then
#il existe 1 ou plusieurs fichiers, on va prendre le premier uniquement
	echo " Package(s) update optiweb trouve(s) $i"
	ipkg install -force-overwrite $i
	cd $CURVERPATH
	IMAGEFILE=""
	for i in cent-optiweb*.ipk
	do
 		IMAGEFILE=$i
	done
#	ls /media/sdcard/curversion/cent-optiweb*.ipk > /dev/null
	if [ -f $i ]; then
		mv -f /media/sdcard/curversion/cent-optiweb*.ipk /media/sdcard/lastversion/
	fi
	mv -f /media/sdcard/updates/cent-optiweb*.ipk /media/sdcard/curversion/
elif [ ! -f /home/heurtaux ]; then
#heurtaux pas present: rootfs neuf et donc reinstaller le ipk dans curversion
#	ls /media/sdcard/curversion/cent-optiweb*.ipk > /dev/null
	cd $CURVERPATH
	IMAGEFILE=""
	for i in cent-optiweb*.ipk
	do
 		IMAGEFILE=$i
	done
	if [ -f $i ]; then
		#il existe 1 ou plusieurs fichiers, on va prendre le premier uniquement
		echo " Recup ancien Package optiweb"
		ipkg install -force-overwrite $i
	fi
fi

#demarrage du surveilleur cable reseau pour reconnection auto
ifplugd &

#verif si une update systeme vient d etre installee. si oui la copie dans curversion
cd $UPDATEPATH
IMAGEFILE=""
for i in linuximage.*.ow
do
 	IMAGEFILE=$i
done
if [ -f $i ]; then
	mv -f /media/sdcard/curversion/linuximage.*.ow /media/sdcard/lastversion/
	mv -f /media/sdcard/updates/linuximage.*.ow /media/sdcard/curversion/
fi

IMAGEFILE=""
for i in rootubifs.*.ow
do
 	IMAGEFILE=$i
done
if [ -f $i ]; then
	mv -f /media/sdcard/curversion/rootubifs.*.ow /media/sdcard/lastversion/
	mv -f /media/sdcard/updates/rootubifs.*.ow /media/sdcard/curversion/
fi

exit 0

