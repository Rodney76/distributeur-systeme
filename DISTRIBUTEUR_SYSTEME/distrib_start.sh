#!/bin/bash


#--------------------------------------------------------------------------
# Verif si cartes SD presentes et montees, sinon les monte
if cat /proc/mounts | grep -F " /media/sdcard " > /dev/null; then
	echo " Carte SD1 deja montee"
else
	mount -o noatime /dev/mmcblk0p1 /media/sdcard
	if [ $? != 0 ]; then
		echo "  Montage SD card 1 echoue :" $?
	else
		echo "  Montage SD card 1 OK"
	fi
fi


#--------------------------------------------------------------------------

sleep 10
echo "--> cursor blink"
/bin/sh -c "echo 0 > /sys/class/graphics/fbcon/cursor_blink"

sleep 7
echo "--> restart getty@tty1"
#/bin/sh -c "systemctl restart getty@tty1"

sleep 1
echo "--> go distributeur"
cd home

# control du brightness
echo 7 > /sys/class/backlight/backlight.10/brightness

# mise a l'heure fuseau horaire                                                                                      
timedatectl set-timezone Europe/Paris                                                                                    


if [ -f /home/READY ]
then
    cp DISTRIBUTEUR OLD_DISTRIBUTEUR
    mv NEW_DISTRIBUTEUR DISTRIBUTEUR
    rm READY	
fi
                                                                                                                                      

./DISTRIBUTEUR


