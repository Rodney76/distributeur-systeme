
mount -o noatime /dev/mmcblk0p1 /media/sdcard

--------------------------------------------------

ifup eth0
devinfo eth0

edit /env/network/eth0

saveenv

barebox_update -t MLO.nand /mnt/tftp/MLO

barebox_update -t nand /mnt/tftp/barebox.bin

erase /dev/nand0.bareboxenv.bb

reset
ifup eth0
-----------------------------------------------------------------------------------------
Now get the Linux kernel and oftree from your TFTP server and store it also into the NAND 
Flash with:  


erase /dev/nand0.kernel.bb
cp /mnt/tftp/linuximage /dev/nand0.kernel.bb

erase /dev/nand0.oftree.bb
cp /mnt/tftp/oftree /dev/nand0.oftree.bb

--------------------------------------------------------------
For flashing Linux’s root filesystem to NAND, please use:  

ubiformat /dev/nand0.root 

ubiattach /dev/nand0.root

ubimkvol /dev/nand0.root.ubi root 0

cp –v /mnt/tftp/root.ubifs /dev/nand0.root.ubi.root

=========================================================================================
5.4.2 Updating from a PTXdist BSP
When updating the images on the NAND Flash from a PTXdist-based release (Linux Kernel
3.2) to a Yocto-based release, there might be an UBIFS issue coming up when mounting the
root filesystem. The old Linux kernel had an issue with subpage write which required to set
the VID header offset to 2048. This is not needed any more. But because of that the UBI
parameters changed which requires a different call:

ubiformat –y -f /dev/nand0.root -s 512
ubiattach /dev/nand0.root
ubimkvol /dev/nand0.root.ubi root 0
cp /mnt/disk/root.ubifs /dev/nand0.root.ubi.root

