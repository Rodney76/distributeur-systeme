#!/bin/bash

#-------------------------------------------------------------------------------------------
# TENTATIVE CONNECTION SUR LE PORT 22000
ssh -o StrictHostKeyChecking=no -fN -R 22000:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow
if [ $? = 0 ]
then
	echo "Connection ssh reussi sur le port 22000"
else
	echo "Echec connection ssh sur le port 22000, test sur le port 22001"

	#-------------------------------------------------------------------------------------------
	# TENTATIVE CONNECTION SUR LE PORT 22001
	ssh -o StrictHostKeyChecking=no -fN -R 22001:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow	
	if [ $? = 0 ]
	then
		echo "Connection ssh reussi sur le port 22001"		
	else
		echo "Echec connection ssh sur le port 22001, test sur le port 22002"
	

		#-------------------------------------------------------------------------------------------
		# TENTATIVE CONNECTION SUR LE PORT 22002
		ssh -o StrictHostKeyChecking=no -fN -R 22002:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow	
		if [ $? = 0 ]
		then
			echo "Connection ssh reussi sur le port 22002"		
		else
			echo "Echec connection ssh sur le port 22002, test sur le port 22003"

			#-------------------------------------------------------------------------------------------
			# TENTATIVE CONNECTION SUR LE PORT 22003
			ssh -o StrictHostKeyChecking=no -fN -R 22003:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow	

			if [ $? = 0 ]
			then
				echo "Connection ssh reussi sur le port 22003"		
			else
				echo "Echec connection ssh sur le port 22003, test sur le port 7004"

				#-------------------------------------------------------------------------------------------
				# TENTATIVE CONNECTION SUR LE PORT 22004
				ssh -o StrictHostKeyChecking=no -fN -R 22004:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow	

				if [ $? = 0 ]
				then
					echo "Connection ssh reussi sur le port 22004"		
				else
					echo "Echec connection sur le port 22004"
				fi
			fi
		fi
	fi
fi


