#!/bin/bash

#ifconfig eth0 192.168.0.113 netmask 255.255.255.0 up

'
phelp()
{
	echo "stopdemo : arret de la demo phytec"
	echo "godemo   : lancement de la demo phytec"
	echo "tscreen  : lancement du touch screen"
}

getdistrib()
{
	scp root@192.168.0.48:/Desktop/DOC_DEV_DISTRIBUTEUR/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR /home
}


stopdemo()
{
	systemctl stop phytec-qtdemo.service;
}

godemo()
{
	/usr/bin/QtDemo -qws &
}

tscreen()
{
	/usr/bin/eGTouchD &
}
'

#phelp
#stopdemo
#tscreen
#cd /home
#./DISTRIBUTEUR
