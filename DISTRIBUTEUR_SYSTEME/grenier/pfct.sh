#!/bin/bash

echo "LANCEMENT DU SCRIPT !"

phelp()
{
	echo "stopdemo : arret de la demo phytec"
	echo "godemo   : lancement de la demo phytec"
	echo "tscreen  : lancement du touch screen"
}

getdistrib()
{
	scp root@192.168.0.48:/Desktop/DOC_DEV_DISTRIBUTEUR/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR /home
}


stopdemo()
{
	systemctl stop phytec-qtdemo.service;
}

godemo()
{
	/usr/bin/QtDemo -qws &
}

tscreen()
{
	/usr/bin/eGTouchD &
}

phelp
