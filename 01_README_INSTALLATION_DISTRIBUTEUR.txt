
copy DISTRIBUTEUR sur serveur conguu
------------------------------------
scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/build/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR root@web-coonguu1.tst.fra.optiweb-access.com:/home


copy DISTRIBUTEUR a partir de webcongu
--------------------------------------
scp -P 22031 -o StrictHostKeyChecking=no DISTRIBUTEUR root@127.0.0.1:/home


Mise a jour de l'heure
----------------------
systemctl restart systemd-timesyncd.service

  

______________________________________________________________________________
APRES AVOIR FLASHER LES IMAGES DU DISTRIBUTEUR SALON (EQUIP AUTO)
______________________________________________________________________________

/!\ Sous barebox
cd /env
gedit config-expansion

- dans le fichier config-expansion

#!/bin/sh
#use this expansion when a capacitive touchscreen is connected
. /env/expansions/am335x-phytec-lcd-018-pcm-953

#use this expansion when a resisitive touchscreen is connected
#. /env/expansions/am335x-phytec-lcd-018-pcm-953-res

#7" display
#of_display_timings -S /panel/display-timings/ETM0700G0DH6

#5.7" display
#of_display_timings -S /panel/display-timings/ETMV570G2DHU

#4.3" display
#of_display_timings -S /panel/display-timings/ETM0430G0DH6

#3.5" display
#of_display_timings -S /panel/display-timings/ETM0350G0DH6

#10.4" display AMPIRE HX
of_display_timings -S /panel/display-timings/AM800600


# NE PAS OUBLIER DE SAUVEGARDER !!!!!
saveenv


reset

#================================================================================================
#========================== (SUR LE MODULE PHYTEC, DISTRIB IKEA) ================================

#---------------------------

mkdir /home/images
mkdir /home/.ssh

# Recuperer l'adresse IP et modifier le .bashrc
-----------------------------------------------

ifconfig eth0

# modif bashrc
# remplacer 192.168.0.15 par l'ip du module phytec "ikea"

lock
exit

# Faire un lock et executer:

ssh-keygen -f "/home/phyvm/.ssh/known_hosts" -R 192.168.0.33  (Mettre la bonne adresse IP)


# Recuperer les fichiers systemes et programmes
-----------------------------------------------
- faire des getsystem
- faire le getdistrib
- faire le getimages

- ATTENTION A LA VIDEO D'ACCUEIL ( BILLET, ... )

getsystem; getdistrib; getimages

# Si probleme de connection ssh avec la cible (man on the middle ) alors taper la commande affichée
# à l'ecran du terminal lors de l'affichage 

# Retour sur la cible
lock
cd /home

#--------- Désactiver/activer les bons services --------

systemctl enable new2dis.service
systemctl enable distrib_fct.service
systemctl disable phytec-qtdemo.service

chmod a+x /home/distrib_start.sh 
chmod a+x /home/distrib_fct.sh



# ----------- Parametrer le reverse ssh ----------------

# /Desktop/HOME_BOULOT3/GESTION_DE_PROJET/distrib3.ods
# Ajouter deux numeros de port ssh non pris (ne doit pas depasser 22099!)
echo "editer le fichier distrib_fct.sh et mettre les bons canaux de communication pour le reverse ssh"
  



#===============================================================================================
#========================== (A FAIRE SUR LA CARTE CIBLE REEL) ================

#---------- Parametrage de la SDCARD -----------------------------------------------------------

mkdir /media/sdcard
mount -o noatime /dev/mmcblk0p1 /media/sdcard
mkdir /media/sdcard/curversion
mkdir /media/sdcard/lastversion
mkdir /media/sdcard/updates 


#---------- Exemple de copie de fichier de micro annexe (Bouton de porte etc..)-----------------

scp fuji_distri.0.2.17.mhx root@192.168.0.33:/media/sdcard/updates/fuji_distri.0.2.17.mhx

scp Fuji_distri.0.2.17.mhx root@192.168.0.33:/media/sdcard/updates/fuji_distri.0.2.17.mhx


#===============================================================================================
#		... PUIS PARAMETRER OPTIWEB ...
#===============================================================================================




Tester le reverse ssh
---------------------
POUR ACCEDER AU SERVEUR OVH:

ssh root@web-coonguu1.tst.fra.optiweb-access.com


POUR ACCEDER A LA CIBLE A PARTIR DU SERVEUR OVH:


SUR LE SERVEUR OVH: > ssh -o StrictHostKeyChecking=no root@127.0.0.1 -p 22000



_________________________________________________________________________________
ANNEXES
_________________________________________________________________________________

=> si le graphisme est doublé, en principe c'est parce qu'il manque une ligne de paramétrage
dans le fichier config-expansion

#10.4" display AMPIRE HX
of_display_timings -S /panel/display-timings/AM800600

______________________________________________
REVERSE_SSH

POUR LES DEUX INTERVENANTS (CIBLE, DESKTOP)
-------------------------------------------

copier le fichier id_rsa_srv_ow dans le repertoire home ou qq part.
creer un fichier "config" dans le repertoire /home/.ssh (faire un saut de ligne a la fin)

contenu:
Host=*.tst.fra.optiweb.access.com
IdentityFile=/home/id_rsa_srv_ow


COTE DISTRIBUTEUR
-----------------

chmod 400 id_rsa_srv_ow 

ssh root@web-coonguu1.tst.fra.optiweb-access.com

SUR LE SERVEUR OVH : 
> ssh -o StrictHostKeyChecking=no root@127.0.0.1 -p 22000



COTE CIBLE
----------

chmod 400 id_rsa_srv_ow


ssh -o StrictHostKeyChecking=no -fN -R 22000:127.0.0.1:22 root@web-coonguu1.tst.fra.optiweb-access.com -i /home/id_rsa_srv_ow

( IL FAUT CHANGER DE NUMERO DE PORT POUR CHAQUE DISTRIBUTEUR )
----------------------------------------------------------------


PB DE CONNEXION:
Man in the middle (permission denied), et pourtant connexion ok via le distrib_start, pas d'erreurs relevees.
--> il faut suivre les instructions affichee par le message d'erreur, c'est à dire retirer la ligne indiquée par le msg dans le fichier indiquée par le msg ( /root/.ssh)



_________________________________________________
BACKLIGHT

root@phycore-am335x-1:/sys/class/backlight/backlight.10# echo 7 > brightness 
(se placer dans le repertoire /sys/class/backlight/backlight.10# ) et taper echo 7 > brightness


_________________________________________________
RESEAU

=> Il faut paramètrer le réseau manuellement (tant que pas fait par yocto)
(ATTENTION JE CROIS QUE C'EST FAIT DANS L'IMAGE DU 28 SEPTEMBRE POUR LE SALON, LE BUS CAN AUSSI)

----------------------
- dans /lib/systemd/network editer le fichier 10-eth0.network et mettre DHCP=yes et l'adrs précedente 
en commentaire.


-----------------------------------------------------------------
PARAMETRAGE DE L'HEURE
-----------------------------------------------------------------
Voir la configuration active
$ timedatectl status

A mettre dans le fichier distrib_start.sh
$ timedatectl set-timezone Europe/Paris


